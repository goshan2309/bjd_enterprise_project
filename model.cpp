#include "model.h"

//////////////////////////////////////////////
Shell::~Shell() {}

double Shell::MaterialVolume() const {
	return 0;
}

double Shell::InnerVolume() const {
	return 0;
}

//////////////////////////////////////////////
SphericalShell::SphericalShell(double outer_radius, double thickness) :

	outer_radius_(outer_radius),
	inner_radius_(outer_radius - thickness),

	thickness_(thickness)

{}

double SphericalShell::MaterialVolume() const {
	return
		SphereVolume(outer_radius_) -
		SphereVolume(inner_radius_);
}

double SphericalShell::InnerVolume() const {
	return SphereVolume(inner_radius_);
}

CylindricalShell::CylindricalShell (double outer_radius, double outer_height, double thickness) :

	outer_radius_(outer_radius),
	outer_height_(outer_height),

	thickness_(thickness),

	inner_radius_(outer_radius - thickness),
	inner_height_(outer_height - 2 * thickness) // 2 not 1

{}

double CylindricalShell::MaterialVolume() const {
	return
		CylinderVolume(outer_radius_, outer_height_) -
		CylinderVolume(inner_radius_, inner_height_);
}

double CylindricalShell::InnerVolume() const {
	return CylinderVolume(inner_radius_, inner_height_);
}


StorageTank::StorageTank(
	Shell& shell,
	Material material,
	Gas gas,
	double gas_pressure,
	double distance_to_object
) :
	shell_(&shell),
	material_(material),
	gas_(gas),
	gas_pressure_(gas_pressure),

	distance_to_object_(distance_to_object)
{}

double StorageTank::VolumeOfMaterial() const {
	return shell_->MaterialVolume();
}

double StorageTank::VolumeOfGas() const {
	return shell_->InnerVolume();
}


double StorageTank::MassOfMaterial() const {
	return material_.density_ * VolumeOfMaterial();
}

double StorageTank::ExplosionEnergy() const {

	double factor1 =
		(gas_pressure_ * VolumeOfGas()) / (gas_.adiabatic_exponent_ - 1.0);

	const double atmosphere_pressure = 100'000;
	double base = atmosphere_pressure / gas_pressure_;
	double exponent = (gas_.adiabatic_exponent_ - 1.0) / gas_.adiabatic_exponent_;

	double factor2 = 1.0 - pow(base, exponent);

	return factor1 * factor2;
}

double StorageTank::ShockWaveEnergy() const {

	double total_energy = ExplosionEnergy();

	return 0.6 * total_energy;
}

double StorageTank::FragmentsEnergy() const {

	double total_energy = ExplosionEnergy();

	return 0.4 * total_energy;
}

double StorageTank::TNTequivalent() const {

	const double q_tnt = 4.52 * 1'000'000;

	double shock_wave_energy = ShockWaveEnergy();

	return shock_wave_energy / q_tnt;
}

double StorageTank::Overpressure() const {

	double tnt_equivalent = TNTequivalent();

	double argument = cbrt(tnt_equivalent) / distance_to_object_;

	auto func = [](double x) {
		return 95 * x + 390 * Sqr(x) + 1300 * Cube(x);
	};

	return func(argument);
}

double StorageTank::FragmentsSpeed() const {

	return sqrt((2 * FragmentsEnergy()) / MassOfMaterial());
}

double StorageTank::MaxRange() const {
	
	const double g = 9.8;

	return Sqr(FragmentsSpeed()) / g;
}

double StorageTank::RealRange() const {

	return 238.0 * cbrt(TNTequivalent());
}

void StorageTank::CalcAll() const {

	using namespace std;

	cout << setprecision(3) << fixed;

	cout << "VolumeOfMaterial: " << VolumeOfMaterial() << endl;

	cout << "MassOfMaterial: " << MassOfMaterial() << endl;

	cout << "VolumeOfGas: " << VolumeOfGas() << endl;

	cout << "ExplosionEnergy: " << ExplosionEnergy() << endl;

	cout << "ShockWaveEnergy: " << ShockWaveEnergy() << endl;

	cout << "FragmentsEnergy: " << FragmentsEnergy() << endl;

	cout << "TNTequivalent: " << TNTequivalent() << endl;

	cout << "Overpressure: " << Overpressure() << endl;

	cout << "FragmentsSpeed: " << FragmentsSpeed() << endl;

	cout << "MaxRange: " << MaxRange() << endl;

	cout << "RealRange: " << RealRange() << endl;
}
