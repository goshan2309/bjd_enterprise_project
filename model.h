#pragma once

#include <string>
#include <iostream>
#include <iomanip>

#include "my_math.h"

class Shell {
protected:
	virtual ~Shell();
public:

	virtual double MaterialVolume() const;

	virtual double InnerVolume() const;

};

class SphericalShell : public Shell {
public:

	SphericalShell(double outer_radius, double thickness);


	double MaterialVolume() const override;

	double InnerVolume() const override;

	double outer_radius_;
	double inner_radius_;

	double thickness_;

};

struct CylindricalShell : public Shell {

	CylindricalShell(double outer_radius, double outer_height, double thickness);

	double MaterialVolume() const override;

	double InnerVolume() const override;

	double outer_radius_;
	double outer_height_;

	double thickness_;

	double inner_radius_;
	double inner_height_;
};

struct Material {
	std::string name_;
	double density_;
};

struct Gas {
	std::string name_;
	double adiabatic_exponent_;
};


class StorageTank {
public:

	StorageTank(
		Shell& shell,
		Material material,
		Gas gas,
		double gas_pressure,
		double distance_to_object
	);

	double VolumeOfMaterial() const;

	double VolumeOfGas() const;

	double MassOfMaterial() const;

	double ExplosionEnergy() const;

	double ShockWaveEnergy() const;

	double FragmentsEnergy() const;

	double TNTequivalent() const;

	double Overpressure() const;

	double FragmentsSpeed() const;

	double MaxRange() const;

	double RealRange() const;

	void CalcAll() const;

	Material material_;
	Shell* shell_;
	Gas gas_;
	double gas_pressure_;

	double distance_to_object_;
};


